import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
 
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @Output() showLogin: EventEmitter<boolean> = new EventEmitter<boolean>();

  registrationForm: FormGroup;
  login: FormControl;
  email: FormControl;
  password: FormControl;

  isAuthorize: boolean = true;

  constructor(private authService: AuthService, private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.createFormControls();
    this.createForm();    
  }

  closeModal() {
    this.showLogin.next(false);
  }

  createForm() {
    this.registrationForm = new FormGroup({
      login: this.login,
      password: this.password,
      email: this.email
    });
  }

  createFormControls() {
    this.login = new FormControl("", [Validators.required, Validators.minLength(4), Validators.maxLength(15), Validators.pattern("[a-zA-Z0-9._-]+")]);
    this.email = new FormControl("temp@temp.ru", [Validators.required, Validators.pattern("^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$")]);
    this.password = new FormControl("", [Validators.required, Validators.minLength(5), Validators.maxLength(20), Validators.pattern("[a-zA-Z0-9.\/_-]+")]);
  }

  onSubmit() {
    if(this.registrationForm.valid) {
      this.authService.authorize(this.registrationForm.value.login, this.registrationForm.value.password);
    };
    if (this.authService.isLogged()) {
      this.showLogin.next(false);
      this.router.navigate(["account"]);
    }
  }

  switchRegister() {
    if (!this.isAuthorize) {
      this.email.setValue("temp@temp.ru");
    } else {
      this.email.setValue("");
    }
    this.isAuthorize = !this.isAuthorize;    
  }

}
