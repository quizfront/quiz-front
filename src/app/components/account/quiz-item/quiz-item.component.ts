import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { QuizAnalitic } from '../../../models/index';

@Component({
  selector: 'quiz-item',
  templateUrl: './quiz-item.component.html',
  styleUrls: ['./quiz-item.component.scss']
})
export class QuizItemComponent implements OnInit {

  // isButtonVisible: boolean = true;
  // isInfoBlock: boolean = true;

  isDisplayNone = false;


  
  @Input() info: QuizAnalitic;

  @Input() isButtonVisible;
  @Input() isInfoBlock;

  @Input() varibleForComparison;
  @Output() _varibleForComparison: EventEmitter<number> = new EventEmitter<number>();

  


  forComparison (val) {
    this.isDisplayNone = !this.isDisplayNone;

    if (this.isDisplayNone) {
      this._varibleForComparison.next(this.varibleForComparison+1);
    }
    else {
      this._varibleForComparison.next(this.varibleForComparison-1);
    }
  }  


  constructor() { }

  ngOnInit() {
  }

}
