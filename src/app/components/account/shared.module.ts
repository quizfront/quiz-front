import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuizItemComponent } from './quiz-item/quiz-item.component';

@NgModule({
    imports: [
        CommonModule
     ],
    declarations: [
         QuizItemComponent
    ],
    exports: [
        QuizItemComponent
    ]
})

export class SharedModule {}