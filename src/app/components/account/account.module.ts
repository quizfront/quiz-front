import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader, TranslatePipe } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AccountRoutingModule } from './account-routing.module';
import { SharedModule } from './shared.module';
import { AnaliticsModule } from './analitics/analitics.module';
import { QuizModule } from './quiz/quiz.module';
import { PaymentsModule } from './payments/payments.module';
import { AuthGuardService } from '../../services/auth-guard.service';

import { SortPipe } from './partners/sort.pipe';

import {
    PartnersComponent,
    AccountComponent,
    TariffsComponent,
    HowToComponent,
    HowToGroupComponent,
    SettingsComponent,
    QuizItemComponent
} from './index';


@NgModule({
    imports: [
        CommonModule,
        AccountRoutingModule,
        FormsModule,
        AnaliticsModule,
        QuizModule,
        PaymentsModule,
        SharedModule,
        TranslateModule
    ],
    declarations: [
        PartnersComponent,
        AccountComponent,
        TariffsComponent,
        HowToComponent,
        HowToGroupComponent,
        SortPipe,
        SettingsComponent
    ],
    exports: [TranslatePipe],
    providers: [TranslateModule]
})

export class AccountModule {}


