import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tariffs',
  templateUrl: './tariffs.component.html',
  styleUrls: ['./tariffs.component.scss']
})
export class TariffsComponent implements OnInit {

  // массив тарифов для tariffs

  tariffs = [
    {
      name: 'Базовый',
      options: [
        {
          ui: [
            { name: 'Настройка фона квиза', avaliable: false },
            { name: 'Возможность загружать фон (картинку)', avaliable: true },
            { name: 'Кадрирование загруженного изображения', avaliable: false }
          ],
          func: [
            { avaliable: true, name: 'Настройка строки прогресса' },
            { avaliable: true, name: 'Настройка размера изображений в вопросах' },
            { avaliable: false, name: 'Настройка прозрачности вариантов ответов' },
            { valavaliableue: false, name: 'Настройка цвета кнопок ответоа' },
            { avaliable: false, name: 'Настройка цвета шрифта кнопок ответов' }
          ]
        }
      ]
    },
    {
      name: 'Брендирование',
      options: [
        {
          ui: [
            { name: 'Настройка фона квиза', avaliable: false },
            { name: 'Возможность загружать фон (картинку)', avaliable: true },
            { name: 'Кадрирование загруженного изображения', avaliable: false }
          ],
          func: [
            { avaliable: true, name: 'Настройка строки прогресса' },
            { avaliable: true, name: 'Настройка размера изображений в вопросах' },
            { avaliable: false, name: 'Настройка прозрачности вариантов ответов' },
            { valavaliableue: false, name: 'Настройка цвета кнопок ответоа' },
            { avaliable: false, name: 'Настройка цвета шрифта кнопок ответов' }
          ]
        }
      ]
    },
    {
      name: 'Максимум конверсии',
      options: [
        {
          ui: [
            { name: 'Настройка фона квиза', avaliable: false },
            { name: 'Возможность загружать фон (картинку)', avaliable: true },
            { name: 'Кадрирование загруженного изображения', avaliable: false }
          ],
          func: [
            { avaliable: true, name: 'Настройка строки прогресса' },
            { avaliable: true, name: 'Настройка размера изображений в вопросах' },
            { avaliable: false, name: 'Настройка прозрачности вариантов ответов' },
            { valavaliableue: false, name: 'Настройка цвета кнопок ответоа' },
            { avaliable: false, name: 'Настройка цвета шрифта кнопок ответов' }
          ]
        }
      ]
    }
  ]





  constructor() { }

  ngOnInit() {
  }





}
