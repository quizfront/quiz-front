import { Component, Input, trigger, state, style, transition, animate } from '@angular/core';

@Component({
    selector: 'how-to',
    templateUrl: './how-to.component.html',
    styleUrls: ['./how-to.component.scss'],
    animations: [
        trigger('infoState', [
            state('closed', style({
              height: '0rem',
              padding: '0',
              'border-bottom': '1px solid transparent'
            })),
            state('opened', style({
              height: 'auto'
            })),
            transition('closed => opened', animate(500)),
            transition('opened => closed', animate(200))
        ])
    ]
})

export class HowToComponent {

    state: string = 'closed';

    @Input() title: string = '';
    @Input() details_text: string = '';

    constructor() {}

    ngOnInit() {}

    toggleInfo() {
        this.state == 'closed' ? this.state = 'opened' : this.state = 'closed';
    }

}