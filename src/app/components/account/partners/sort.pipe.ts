import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {
     transform(affilatesData, value : string, upOrDown:boolean) {

        if( upOrDown ) {
          return this.sortColumUp ( affilatesData, value );
        }
        else {
          return this.sortColumDown ( affilatesData, value );
        }
    }


    sortDate ( affilatesData, value ) {
        alert (value);
    }


    sortColumDown ( affilatesData, value) {
      return affilatesData.sort(function(a, b) {
            if (a[value] > b[value]) {
                return 1;
            }
            if (a[value] < b[value]) {
                return -1;
            }
            return 0;
        })
    }

    sortColumUp ( affilatesData, value) {
      return affilatesData.sort(function(a, b) {
            if (a[value] < b[value]) {
                return 1;
            }
            if (a[value] > b[value]) {
                return -1;
            }
            return 0;
        })
    }
}
