import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/index';


@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss']
})
export class PartnersComponent implements OnInit {



// переменные для иконок сортировки (на каждое поле)
  isFilterOnName: boolean = false;
  isFilterOnQuiz: boolean = true;
  isFilterOnPaidQuiz: boolean = true;
  isFilterOnProfit: boolean = true;
// отображение баланса
  balance: number = 1200;
// массив данных об афилатах(кто-бы они ни были)
  affilatesData = [];
  beforAfillData = [];
  nowDate = new Date().getTime();
// переменные для пайпа
  sortVar : string = 'name';
  upOrDown : boolean = false;
// переменная даты(ден или неделя или..)
  dateCategory: number = 3;




  constructor(private authService : AuthService) { }

  ngOnInit() {
    this.beforAfillData = this.authService.authAffilatesData;
     this.filterDay();
  }


// фильтр по дате
   filterDay() {
     if (this.dateCategory == 3) {

       for (let i = 0; i <this.beforAfillData.length; i++ ) {
         this.affilatesData[i] = this.beforAfillData[i];
       }
     }
     if (this.dateCategory == 2) {
       this.affilatesData = [];
       for (let i = 0; i <this.beforAfillData.length; i++ ) {
          if ( this.quantityDay( i ) <= 32) {
            this.affilatesData.push(this.beforAfillData[i]);
          }
       }
     }
     if (this.dateCategory == 1) {
       this.affilatesData = [];
       for (let i = 0; i <this.beforAfillData.length; i++ ) {
          if ( this.quantityDay( i ) < 8) {
            this.affilatesData.push(this.beforAfillData[i]);
          }
       }
     }
     if (this.dateCategory == 0) {
       this.affilatesData = [];
       for (let i = 0; i <this.beforAfillData.length; i++ ) {
          if ( this.quantityDay( i ) < 2) {
            this.affilatesData.push(this.beforAfillData[i]);
          }
       }
     }
   }


  quantityDay ( i ) {
      let d = Date.parse(this.beforAfillData[i].date);
      return (this.nowDate - d)/86400000;
}

// иконка фильтра сортировки

runFilterIcon( value: string ) {
    if ( value == 'name')
    {this.isFilterOnName = !this.isFilterOnName;}
    else { this.isFilterOnName = true }

    if ( value == 'quiz')
    {this.isFilterOnQuiz = !this.isFilterOnQuiz;}
    else { this.isFilterOnQuiz = true }

    if ( value == 'paid_quiz')
    {this.isFilterOnPaidQuiz = !this.isFilterOnPaidQuiz;}
    else { this.isFilterOnPaidQuiz = true }

    if ( value == 'profit')
    {this.isFilterOnProfit = !this.isFilterOnProfit;}
    else { this.isFilterOnProfit = true }
}


// для пайпа фильтра сортировки
  runFilter(sortVar) {
    if (this.sortVar == sortVar) {
      if (this.upOrDown !== true) {
        this.upOrDown = true;
      }
      else {this.upOrDown = false;}

    }
    else { this.upOrDown = false; }


    this.sortVar = sortVar;
  }



}
