import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import { AccountModule } from '../account.module';

import { SharedModule } from '../shared.module';

import {
    QuizCompareComponent,
    QuizAnaliticListComponent,
    QuizViewComponent
} from './index';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule,
        SharedModule   
    ],
    declarations: [
        QuizAnaliticListComponent
    ]
})

export class AnaliticsModule {}