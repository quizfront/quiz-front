import { Component, OnInit } from '@angular/core';

import { QuizAnalitic } from '../../../../models/index';

@Component({
  selector: 'app-quiz-analitic-list',
  templateUrl: './quiz-analitic-list.component.html',
  styleUrls: ['./quiz-analitic-list.component.scss']
})
export class QuizAnaliticListComponent implements OnInit {

  isButtonVisible: boolean = true;
  isInfoBlock: boolean = true;
  varibleForComparison: number = 0;

  varibleForComp(event) {
    this.varibleForComparison =+ event;
  }


  quiz: QuizAnalitic[] = [
  {
    id: 0,
    image: './assets/images/bg_quiz-item/fon1.png',
    name: 'Quiz Name',
    coupon: 0,
    payed: '01.01.18',
    trialPeriod: 8,
    cr: 0,
    request: 0,
    tariff: 'No tariff'
  },
  {
    id: 0,
    image: './assets/images/bg_quiz-item/fon2.png',
    name: 'Quiz Name',
    coupon: 0,
    payed: '0',
    trialPeriod: 8,
    cr: 0,
    request: 0,
    tariff: 'No tariff'
  },
  {
    id: 0,
    image: './assets/images/bg_quiz-item/fon3.png',
    name: 'Quiz Name',
    coupon: 0,
    payed: '01.01.18',
    trialPeriod: 8,
    cr: 0,
    request: 0,
    tariff: 'No tariff'
  },
  {
    id: 0,
    image: './assets/images/bg_quiz-item/fon4.png',
    name: 'Quiz Name',
    coupon: 0,
    payed: '01.01.18',
    trialPeriod: 8,
    cr: 0,
    request: 0,
    tariff: 'No tariff'
  },
  {
    id: 0,
    image: './assets/images/bg_quiz-item/fon5.png',
    name: 'Quiz Name',
    coupon: 0,
    payed: '01.01.18',
    trialPeriod: 8,
    cr: 0,
    request: 0,
    tariff: 'No tariff'
  }
]



  constructor() { }

  ngOnInit() {
  }

}
