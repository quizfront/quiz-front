import { Component, OnInit } from '@angular/core';
import { Settings } from './settings.interface';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

lastname: string;
patronymic: string;
firstname: string;
login: string;
password: number;

setting: Settings = {
  ip: 13354345,
  login: 'security',
  password:  123,
  firstname:  "Александр",
  lastname:  undefined,
  patronymic:  'Радионович'
};


  constructor() { }

  ngOnInit() {}

  runSettings() {
    if (this.lastname !== undefined) {this.setting.lastname = this.lastname;}
    if (this.patronymic !== undefined) {this.setting.patronymic = this.patronymic;}
    if (this.firstname !== undefined) {this.setting.firstname = this.firstname;}
    if (this.login!== undefined) {this.setting.login = this.login;}
    if (this.password !== undefined) {this.setting.password = this.password;}
  }

}
