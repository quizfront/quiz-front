export * from './tariffs/tariffs.component';
export * from './partners/partners.component';
export * from './how-to-group/how-to/how-to.component';
export * from './how-to-group/how-to-group.component';
export * from './quiz-item/quiz-item.component';
export * from './account.component';
export * from './settings/settings.component';
