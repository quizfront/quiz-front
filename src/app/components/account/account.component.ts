import { Component, OnInit,  ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../services/index';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  @ViewChild('menu') menu: ElementRef;
  @ViewChild('toggleArrow') arrow: ElementRef;

  private showMenu: boolean = true;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {}

  username: string = 'Admin';

  logout() {
    this.authService.logout();
    this.router.navigate(["home"]);
  }

}
