import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Transaction } from '../../../../models/index';

@Component({
  selector: 'app-pay-confirm',
  templateUrl: './pay-confirm.component.html',
  styleUrls: ['./pay-confirm.component.scss']
})
export class PayConfirmComponent implements OnInit {

  transaction: Transaction = {
    id: 0,
    number: 356,
    pay_way: 'Yandex',
    comments: 'Lorem ipsum some comments',
    coupon: null,
    user: {
      id: 0,
      name: 'Dmitry',
      phone: '+001234567',
      email: 'test@emai.com',
      comments: 'Lorem ipsum some comments'
    },
    quiz: {
      id: 0,
      name: 'My Quiz',
      cost: 1000,
      type: 'Quiz button',
      tariff: 'Base'
    }
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.router.navigate(['/account/payments/pay-complete'])
  }

}
