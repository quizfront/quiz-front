import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import {
    PayCompleteComponent,
    PayConfirmComponent,
    PaymentListComponent,
    PayProcessComponent
} from './index';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        RouterModule
    ],
    declarations: [
        PayCompleteComponent,
        PayConfirmComponent,
        PaymentListComponent,
        PayProcessComponent
    ],
    exports: [RouterModule]
})

export class PaymentsModule {}