import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { PaymentItem } from '../../../../models/index';

@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.scss']
})
export class PaymentListComponent implements OnInit {

  paymentList: PaymentItem[] = [
    {
      id: 0,
      date: '15.05.2018',
      name: 'Base',
      status: 'Payed',
      summ: 1000,
      type: 'Quiz-landing'
    },
    {
      id: 1,
      date: '10.01.2018',
      name: 'Full',
      status: 'Ended',
      summ: 500,
      type: 'Quiz-button'
    },
    {
      id: 2,
      date: '10.04.2018',
      name: 'Full',
      status: 'Not payed',
      summ: 500,
      type: 'Quiz-button'
    }
  ];

  sortSelected = {
    id: false,
    date: false,
    name: false,
    status: false,
    summ: false,
    type: false
  }

  constructor(private router: Router) {}

  ngOnInit() {}

  sortBy(type: string) {
    this.paymentList.sort((a, b) => {
      if (a[type] > b[type]) {
        if(this.sortSelected) return 1
          return -1
      }
      if (a[type] < b[type]) {
        if(this.sortSelected) return 1
          return -1
      }
      if (a[type] == b[type]) return 0;
    })       
  }

  buyQuiz() {
    this.router.navigate(['account/payments/pay-process']);
  }

}
