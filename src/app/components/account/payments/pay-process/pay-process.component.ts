import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pay-process',
  templateUrl: './pay-process.component.html',
  styleUrls: ['./pay-process.component.scss']
})
export class PayProcessComponent implements OnInit {

  contactForm: FormGroup;

  name: FormControl;
  phone: FormControl;
  email: FormControl;
  comments: FormControl;
  
  quizName: string = 'My Quize';
  tariff = {
    name: 'Base',
    cost: '5 euro in minute'
  }

  constructor(private router: Router) { }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
  }

  createForm() {
    this.contactForm = new FormGroup({
      name: this.name,
      phone: this.phone,
      email: this.email,
      comments: this.comments
    })
  }

  createFormControls() {
    this.name = new FormControl("", [Validators.required, Validators.minLength(5), Validators.maxLength(50)]);
    this.phone = new FormControl("");
    this.email = new FormControl("", [Validators.required, Validators.pattern("^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$")]);
    this.comments = new FormControl("", Validators.maxLength(500));
  }

  onSubmit() {
    if(this.contactForm.valid) {
      console.log('Form valid');
    }
    this.router.navigate(['/account/payments/pay-confirm']);
  }

}
