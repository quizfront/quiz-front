import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

// TRANSLATE MODULES
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader, TranslatePipe } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// COLOR PICKER MODULE
import { ColorPickerModule } from 'ngx-color-picker';

import { SharedModule } from '../shared.module';


import { 
    ModalGroupComponent,
    ModalFaqComponent,
    ModalSettingsComponent,
    AddItemComponent,
    AddTextComponent,
    AddButtonComponent,
    ScrollBarComponent,
    ColorPickerComponent,
    AngleRadiusComponent,
    FontSelectorComponent,
    EffectComponent
} from '../../../shared/index';

import { MenuSettingsComponent } from '../../../shared/menu-settings/menu-settings.component';


import {
    MyQuizListComponent,
    ViewQuizComponent,
    TemplateListComponent,
    CreateQuizComponent,
    SectionComponent,
    DomElementComponent
} from './index';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule,
        SharedModule,
        FormsModule,
        TranslateModule,
        ColorPickerModule
    ],
    declarations: [
        MyQuizListComponent,
        ViewQuizComponent,
        TemplateListComponent,
        CreateQuizComponent,
        ModalFaqComponent,
        ModalGroupComponent,
        ModalSettingsComponent,
        MenuSettingsComponent,
        SectionComponent,
        AddItemComponent,
        AddTextComponent,
        AddButtonComponent,
        ScrollBarComponent,
        ColorPickerComponent,
        AngleRadiusComponent,
        FontSelectorComponent,
        EffectComponent,
        DomElementComponent
    ],
    exports: [
        RouterModule,
        TranslatePipe
    ],
    providers: [TranslateModule]
})

export class QuizModule {}