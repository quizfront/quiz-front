import { Component, Input } from '@angular/core';

import { QuizService, ViewService } from '../../../../services/index';

import { Section } from '../../../../models/index';

@Component({
    selector: 'section',
    templateUrl: './section.component.html',
    styleUrls: ['./section.component.scss'],
})

export class SectionComponent {

    @Input() section: Section;

    cols: string = '6';
    blocks: number[];

    constructor(private qs: QuizService, private vs: ViewService) {}

    ngOnInit() {
        this.cols = this.getBootstrapCols();
        this.blocks = this.getBlockArray();
    }

    getBootstrapCols():string {
        return (12 / this.section.blocks.length).toString();
    }

    getBlockArray():number[] {
        let t: number[] = [];
        for(let i=0;i<this.section.blocks.length; i++) {
            t.push(i);
        }
        return t;
    }

    addSection(number: number) {
        this.qs.addSection(number);
    }

}