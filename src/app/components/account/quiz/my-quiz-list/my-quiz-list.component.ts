import { Component, OnInit } from '@angular/core';

import { QuizAnalitic } from '../../../../models/index';

@Component({
  selector: 'my-quiz-list',
  templateUrl: './my-quiz-list.component.html',
  styleUrls: ['./my-quiz-list.component.scss']
})
export class MyQuizListComponent implements OnInit {

  isButtonVisible: boolean = false;
  isInfoBlock = true;

  quizs: QuizAnalitic[] = [
    {
      id: 0,
      image: './assets/images/quiz_thumb.jpg',
      name: 'Quiz Name',
      coupon: 0,
      payed: '01.01.18',
      trialPeriod: 8,
      cr: 0,
      request: 0,
      tariff: 'No tariff'
    },
    {
      id: 1,
      image: './assets/images/quiz_thumb.jpg',
      name: 'Quiz Name',
      coupon: 0,
      payed: '01.01.18',
      trialPeriod: 8,
      cr: 0,
      request: 0,
      tariff: 'No tariff'
    },
    {
      id: 2,
      image: './assets/images/quiz_thumb.jpg',
      name: 'Quiz Name',
      coupon: 0,
      payed: '01.01.18',
      trialPeriod: 8,
      cr: 0,
      request: 0,
      tariff: 'No tariff'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
