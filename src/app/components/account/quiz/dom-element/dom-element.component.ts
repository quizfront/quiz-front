import { Component, Input } from '@angular/core';

import { QuizService } from '../../../../services/index';

@Component({
    selector: 'dom-element',
    templateUrl: 'dom-element.component.html',
    styleUrls: ['dom-element.component.scss']
})

export class DomElementComponent {

    @Input() element: any;
    @Input() index: any;

    constructor(private qs: QuizService) {}

    ngOnInit() {
        console.log(this.element);
    }

}