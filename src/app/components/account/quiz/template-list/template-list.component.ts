import { Component, OnInit, ViewChildren, ElementRef } from '@angular/core';

import * as $ from 'jquery';

import { ViewService } from '../../../../services/index';

@Component({
  selector: 'template-list',
  templateUrl: './template-list.component.html',
  styleUrls: ['./template-list.component.scss']
})
export class TemplateListComponent implements OnInit {

  categories: string[] = [];
  templates: any[] = [];
  selectedCategory: string = 'Все шаблоны';

  @ViewChildren('chevron') chevrons;

  isShowItems: boolean = true;

  quizTemplate = [
    {
      topic: 'Автомеханика',
      name: 'СТО - Ремонт авто',
      picture: './assets/images/quiz.jpg'
    },
    {
      topic: 'Автомеханика',
      name: 'Официальный диллер Honda',
      picture: './assets/images/quiz.jpg'
    },
    {
      topic: 'Автомеханика',
      name: 'Официальный диллер Honda',
      picture: './assets/images/quiz.jpg'
    },
    {
      topic: 'Автомеханика',
      name: 'Официальный диллер Honda',
      picture: './assets/images/quiz.jpg'
    },
    {
      topic: 'Рестораны/Еда',
      name: 'Вкусно и полезно',
      picture: './assets/images/quiz.jpg'
    },
    {
      topic: 'Рестораны/Еда',
      name: 'Пицца 24/7',
      picture: './assets/images/quiz.jpg'
    },
    {
      topic: 'Мультимедиа/Техника',
      name: 'Apple и не только',
      picture: './assets/images/quiz.jpg'
    },
  ]

  constructor(private view: ViewService) { }

  ngOnInit() {
    this.createUniqueCategories();
    this.templates = this.quizTemplate;
  }

  createUniqueCategories() {
    this.quizTemplate.forEach(elem => {
      if (!this.categories.some(cat => cat == elem.topic)) {
        this.categories.push(elem.topic);
      }
    });
  }

  getQuizsByTopic(topic: string) {
    let quizs: any[] = [];
    this.templates.forEach(elem => {
      if (elem.topic == topic) {
        quizs.push(elem);
      }
    });
    return quizs;
  }

  filterBy(category: string) {
    if (category == 'Все шаблоны') {
      this.templates = this.quizTemplate;
    } else {
      this.templates = [];
      this.selectedCategory = category;
      this.quizTemplate.forEach(elem => {
        if (elem.topic == category) {
          this.templates.push(elem);
        }
      });
    }    
  }

  toggleItems(i: number) {
    $('.content__items:eq(' + i + ')').slideToggle();
    let chevron = this.chevrons._results[i].nativeElement.style.transform;
    if (chevron == 'rotate3d(1, 0, 0, 0deg)') {
      this.chevrons._results[i].nativeElement.style.transform = 'rotate3d(1, 0, 0, 180deg)';
    } else {
      this.chevrons._results[i].nativeElement.style.transform = 'rotate3d(1, 0, 0, 0deg)';
    }
  }

}