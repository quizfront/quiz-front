import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { ViewService, LocalStorageService, QuizService } from '../../../../services/index';

import { MainSettings, Section } from '../../../../models/index';

@Component({
    selector: 'create-quiz',
    templateUrl: './create-quiz.component.html',
    styleUrls: ['./create-quiz.component.scss']
})
export class CreateQuizComponent implements OnInit {

    modalType: string = 'faq';

    showMenu: boolean = false;
    isShowFaq: boolean = false;
    isShowSettings: boolean = false;

    @ViewChild('submenu') submenu: ElementRef;
    @ViewChild('toggleArrow') arrow: ElementRef;
    @ViewChild('faq') faq: ElementRef;
    @ViewChild('settings') settings: ElementRef;

    quiz = {};

    constructor(private view: ViewService, private ls: LocalStorageService, private qs: QuizService) {
        this.qs.quizUpdated.subscribe(data => {
            this.quiz = this.qs.quiz;
        });
    }

    ngOnInit() {
        this.checkNeedFaq(); // Проверка отображать окно подсказок или нет
        this.quiz = this.qs.quiz;
    }

    ngAfterViewInit() {
        this.view.hideMenu(); // Прячем меню

    }

    ngOnDestroy() {
        this.view.showMenu();
    }


    // VISUAL METHODS

    checkNeedFaq() {
        if (this.ls.isNeedFaq()) {
            this.isShowFaq = true;
            this.faq.nativeElement.style.zIndex = 7;
        }
    }

    toggleMenu() {
        if (!this.showMenu) {
            this.view.showMenu();
            this.submenu.nativeElement.style.transform = "translateY(9.8rem)";
            this.arrow.nativeElement.style.transform = "translateX(-49px) rotateZ(180deg)";
        } else {
            this.view.hideMenu();
            this.submenu.nativeElement.style.transform = 'translateY(0)';
            this.arrow.nativeElement.style.transform = "translateX(-49px) rotateZ(0)";
        }
        this.showMenu = !this.showMenu;
    }

    showFaq() {
        this.isShowFaq = !this.isShowFaq;
        this.modalType = 'faq';
        if (this.isShowFaq) {
            this.faq.nativeElement.style.zIndex = 7;
        } else {
            this.faq.nativeElement.style.zIndex = 0;
        }
    }

    showSettings() {
        this.isShowSettings = !this.isShowSettings;
        this.modalType = 'settings';
        if (this.isShowSettings) {
            this.settings.nativeElement.style.zIndex = 7;
        } else {
            this.settings.nativeElement.style.zIndex = 0;
        }
    }

    // LOGICAL METHODS

    getModalSettings(data: MainSettings) {
        this.isShowSettings = false;
    }

    saveChanges() {
        let temp = document.getElementsByClassName('workzone')[0].innerHTML;
        temp = temp.replace(/(<!--)[\d\w\s={}"-:\[\]]*(-->)/g, ""); // удаляем комментарии
        temp = temp.replace(/(_[a-z]*-)[a-z0-9]+[="]+/g, ""); // удаляем ng комментарии
        console.log(this.quiz);
    }

}
