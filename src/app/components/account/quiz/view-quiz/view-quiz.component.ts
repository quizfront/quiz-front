import { Component, ViewChild, ElementRef } from '@angular/core';

import { ViewService } from '../../../../services/index';

@Component({
    selector: 'view-quiz',
    templateUrl: './view-quiz.component.html',
    styleUrls: ['./view-quiz.component.scss']
})

export class ViewQuizComponent {

    showMenu: boolean = false;
    @ViewChild('submenu') submenu: ElementRef;
    @ViewChild('toggleArrow') arrow: ElementRef;

    constructor(private view: ViewService) {

    }

    ngAfterViewInit() {    
        this.view.hideMenu();
    }

    ngOnDestroy() {
        this.view.showMenu();
    }

    toggleMenu() {
        if (!this.showMenu) {
            this.view.showMenu();
            this.submenu.nativeElement.style.transform = "translateY(9.8rem)";
            this.arrow.nativeElement.style.transform = "translateX(-49px) rotateZ(180deg)";
        } else {
            this.view.hideMenu();
            this.submenu.nativeElement.style.transform = 'translateY(0)';
            this.arrow.nativeElement.style.transform = "translateX(-49px) rotateZ(0)";
        }
        this.showMenu = !this.showMenu;        
    }
}
