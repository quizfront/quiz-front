import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuthGuardService } from '../../services/auth-guard.service';

import {
    AccountComponent,
    PartnersComponent,
    TariffsComponent,
    HowToGroupComponent,
    SettingsComponent
} from './index';

import {
    PayCompleteComponent,
    PayConfirmComponent,
    PaymentListComponent,
    PayProcessComponent
} from './payments/index';

import {
 QuizCompareComponent,
 QuizAnaliticListComponent,
 QuizViewComponent
} from './analitics/index';

import {
    MyQuizListComponent,
    ViewQuizComponent,
    TemplateListComponent,
    CreateQuizComponent
} from './quiz/index';


@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: "account",
                component: AccountComponent,
                canActivate: [AuthGuardService],
                children: [
                    {
                        path: "",
                        redirectTo: "template-list",
                        pathMatch: "full"
                    },
                    {
                        path: "how-to",
                        component: HowToGroupComponent
                    },
                    {
                        path: "partners",
                        component: PartnersComponent
                    },
                    {
                        path: "analitics",
                        children: [
                            {
                                path: "",
                                redirectTo: "quiz-list",
                                pathMatch: "full"
                            },
                            {
                                path: "quiz-list",
                                component: QuizAnaliticListComponent
                            }
                        ]
                    },
                    {
                        path: "tariffs",
                        component: TariffsComponent
                    },
                    {
                        path: "my-quiz-list",
                        component: MyQuizListComponent
                    },
                    {
                        path: "template-list",
                        component: TemplateListComponent
                    },
                    {
                        path: 'view-quiz',
                        component:ViewQuizComponent
                    },
                    {
                        path: 'create-quiz',
                        component: CreateQuizComponent
                    },
                    {
                        path: "settings",
                        component: SettingsComponent
                    },
                    {
                        path: "payments",
                        children: [
                            {
                                path: "",
                                redirectTo: "payment-list",
                                pathMatch: "full"
                            },
                            {
                                path: "payment-list",
                                component: PaymentListComponent
                            },
                            {
                                path: "pay-process",
                                component: PayProcessComponent
                            },
                            {
                                path: "pay-confirm",
                                component: PayConfirmComponent
                            },
                            {
                                path: "pay-complete",
                                component: PayCompleteComponent
                            }
                        ]
                    }
                ]
            }
        ])
    ],
    exports: [ RouterModule ]
})

export class AccountRoutingModule {}
