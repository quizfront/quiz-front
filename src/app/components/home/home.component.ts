import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../services/index';

import { Comment } from './comment.interface'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {



  showLoginForm: boolean = false;
  isLogged: boolean = false;

  commentators: Comment[] = [
    {
      image: "./assets/images/testImg.jpg",
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo'
   },
    {
      image: "./assets/images/testImg.jpg",
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo'
   },
    {
      image: "./assets/images/testImg.jpg",
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo'
   },
    {
      image:"./assets/images/testImg.jpg",
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo'
   },
]

  constructor(private authService: AuthService, private router: Router) {
    this.isLogged = authService.isLogged();
   }

  ngOnInit() {}

  switchLogin(isShow: boolean = true) {
    if (this.isLogged) {
      this.router.navigate(["account"]);
    } else {
      this.showLoginForm = isShow;
    }
  }

  scroll(el) {
      el.scrollIntoView();
  }


}
