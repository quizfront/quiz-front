export interface Comment {
  image: string;
  text: string;
}
