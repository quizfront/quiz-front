import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'angle-radius',
    templateUrl: 'angle-radius.component.html',
    styleUrls: ['angle-radius.component.scss']
})

export class AngleRadiusComponent {

    @Input() current: number = 50;
    @Input() max: number = 50;

    @Output() changeRadius: EventEmitter<any>= new EventEmitter<any>();

    angleCheck: boolean[] = [true, true, true, true];

    radius: number = 0;

    constructor() {}

    ngOnInit() {
        this.getRadius();
    }

    onBorderRadiusChange(value: number = this.current) {
        this.current = value;
        this.getRadius();
        this.changeRadius.next(this.getStyleArray());
    }

    getRadius() {
        this.radius = Math.round((1.25*this.current/this.max)*100)/100;
    }

    getStyleArray() {
        let styles: string[] = [];
        this.angleCheck.forEach(elem => {
            if (elem) {
                styles.push(this.radius+'rem');
            } else {
                styles.push('0');
            }
        });
        return styles;
    }
}