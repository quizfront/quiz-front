import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'color-picker',
    templateUrl: 'color-picker.component.html',
    styleUrls: ['color-picker.component.scss']
})

export class ColorPickerComponent {

    preColor = ['#000000', '#ffffff', '#fa1205', '#bbbbaa'];

    selectedColor: string = '#000000';

    @Input() name: string = "color";

    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();

    constructor() {}

    ngOnInit() {}

    onColorPickerChange(color: string) {
        this.onChange.next(color);
        this.selectedColor = color;
    }

    onColorPalleteChange() {
        this.onChange.next(this.selectedColor);
    }


}