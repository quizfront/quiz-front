import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'scroll-bar',
  templateUrl: './scroll-bar.component.html',
  styleUrls: ['./scroll-bar.component.scss']
})
export class ScrollBarComponent implements OnInit {  

  @Output() onChange: EventEmitter<number> = new EventEmitter<number>();
  
  @Input() type: string = "standart";
  @Input() max: number = 100;
  @Input() current: number = 0;

  private middle: number = 50;
  private scale: string = "";

  ngOnInit() {
    this.middle = Math.round(this.max / 2);
    this.initScale();
  }

  changeValue(event) {
    this.onChange.next(event.target.value);
  }

  initScale() {
    this.type=="standart"?this.scale="":this.scale="%";
  }

}
