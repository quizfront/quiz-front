import { Component, Input } from '@angular/core';

import { ViewService, QuizService } from '../../services/index';

@Component({
    selector: 'menu-settings',
    templateUrl: 'menu-settings.component.html',
    styleUrls: ['menu-settings.component.scss']
})

export class MenuSettingsComponent {

    @Input() index: number;

    sectionHeight: number = null;

    constructor(private vs: ViewService, private qs: QuizService) {}

    ngOnInit() {
        this.sectionHeight = this.qs.quiz.sections[this.index].height;
    }

    toggleSettings() {
        this.vs.toggleSettingsMenu(this.index);
    }

    save() {
        this.qs.quiz.sections[this.index].height = this.sectionHeight;
        this.cancel();
    }

    cancel() {
        this.vs.closeDropdown(this.index);
    }
    
}