import { Component, Input, ViewChild, ElementRef } from '@angular/core';

import { LocalStorageService } from '../../../services/index';

import * as $ from 'jquery';

@Component({
    selector: 'modal-faq',
    templateUrl: './modal-faq.component.html',
    styleUrls: ['./modal-faq.component.scss']
})

export class ModalFaqComponent {

    @ViewChild('checkbox') checkbox: ElementRef;

    constructor(private ls: LocalStorageService) {}

    ngOnInit() {
        this.slideUpAll();
        this.needFaq();
    }

    slideUpAll() {
        $('.faq__group-item-text').slideToggle();
    }

    toggleItem(index: number) {
        $('.faq__group-item-text:eq('+index+')').slideToggle();
    }

    needFaq() {
        if(this.ls.isNeedFaq()) {
            this.checkbox.nativeElement.checked = false;
        } else {
            this.checkbox.nativeElement.checked = true;
        }
    }

    switchFaq() {
        if (this.checkbox.nativeElement.checked) {
            this.ls.setNeedFaq();
        } else {
            this.ls.cancelFaq();
        }
    }
}