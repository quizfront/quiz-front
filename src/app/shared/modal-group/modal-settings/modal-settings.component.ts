import { Component, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';

import { MainSettings } from '../../../models/index';

import * as $ from 'jquery';

@Component({
    selector: 'modal-settings',
    templateUrl: './modal-settings.component.html',
    styleUrls: ['./modal-settings.component.scss']
})

export class ModalSettingsComponent {

    settings: MainSettings = {
        font: {
            bold: true,
            italic: false,
            align: 'center',
            family: 'Gotham Bold',
            size: 8,
            color: '#000000'
        },
        background: {            
            color: '#ffffff',
            showGrid: true
        }
    }

    @Output() sendSettings: EventEmitter<MainSettings> = new EventEmitter<MainSettings>();

    outputSettings: MainSettings;

    preColor = ['#000000', '#ffffff', '#fa1205', '#bbbbaa'];

    ngOnInit() {
        this.menuInitialize();
        this.outputSettings = this.settings;
    }

    menuInitialize() {
        $('.settings__content').not(':eq(0)').slideUp();
    }

    toggleItems(index: number) {
        $('.settings__content:eq('+index+')').slideToggle();
        $('.settings__content').not(':eq('+index+')').slideUp();
    }

    changeColor(color: string) {
        this.outputSettings.font.color = color;
    }

    changeBackground(color: string) {
        this.outputSettings.background.color = color;
    }

    saveSettings() {
        this.sendSettings.next(this.outputSettings);
    }

    cancelSettings() {
        this.sendSettings.next(this.settings);
    }
    
}