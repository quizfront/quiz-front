import { Component, Input, Output, EventEmitter } from '@angular/core';

import { MainSettings } from '../../models/index';

@Component({
    selector: 'modal-group',
    templateUrl: './modal-group.component.html',
    styleUrls: ['./modal-group.component.scss']
})

export class ModalGroupComponent {

    @Input() type: string;

    @Output() settings: EventEmitter<MainSettings> = new EventEmitter<MainSettings>();

    position: any;

    constructor() {}

    sendSettings(data: MainSettings) {
        this.settings.next(data);
    }

}