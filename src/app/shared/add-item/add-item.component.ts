import { Component, Input } from '@angular/core';

import { ViewService, QuizService } from '../../services/index';

import * as $ from 'jquery';

@Component({
    selector: 'add-item',
    templateUrl: 'add-item.component.html',
    styleUrls: ['add-item.component.scss']
})

export class AddItemComponent {

    @Input() index: {section: number; block: number};
    @Input() isLast: boolean = false;

    constructor(private vs: ViewService, private qs: QuizService) {
        
    }

    ngOnInit() {
        
    }

    ngAfterViewInit() {
        $('.dropdown').on("click", ".dropdown-menu", function (e) {  
            e.preventDefault();
            e.stopPropagation() });
    }

    toggleMenu() {
        this.vs.hideAllSettings();
        console.log(this.index);
    }

}
