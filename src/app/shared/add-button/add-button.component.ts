import { Component, Input } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';

@Component({
    selector: 'add-button',
    templateUrl: 'add-button.component.html',
    styleUrls: ['add-button.component.scss']
})

export class AddButtonComponent {

    @Input() index: {section:number; block: number};

    button = {
        text: 'Начать тест',
        background: '#2065a0',
        opacity: 1,
        color: '#ffffff',
        height: 2.5,
        radius: ['1.25rem', '1.25rem', '1.25rem', '1.25rem'],
        border_color: '#ffffff',
        border: 0,
        font_family: 'Gotham Bold',
        font_size: 10
    }

    preColor = ['#000000', '#ffffff', '#fa1205', '#bbbbaa'];

    constructor() {}

    ngOnInit() {}

    onButtonRadiusChange(value: string[]) {
        this.button.radius = value;
    }

    onButtonColorChange(color: string) {
        this.button.background = color;
    }

    onBorderColorChange(color: string) {
        this.button.border_color = color;
    }

    onBorderHeightChange(value: number) {
        this.button.border = value;
    }

    onButtonOpacityChange(value: number) {
        this.button.opacity = value / 100;
    }

}