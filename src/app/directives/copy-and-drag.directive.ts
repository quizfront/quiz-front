import { Directive, ElementRef, Renderer2, HostListener, Input } from "@angular/core";

@Directive({
    selector: "[copyAndDrag]"
})

export class CopyAndDragDirective {

    @Input() type: string = "text";

    elementCopy: HTMLBodyElement = null;
    catchElement: boolean = false;

    dropTargetElements: NodeListOf<Element>;

    constructor(
        private elementRef: ElementRef,
        private renderer: Renderer2) {}

        moveElementHandler = (event) => {
            this.moveElement(event);
        }

        mouseUpHandler = (event) => {
            let elementCenter = {
                x: event.clientX + event.srcElement.clientWidth,
                y: event.clientY + event.srcElement.clientHeight
            }
            for (let i=0; i<this.dropTargetElements.length; i++) {
                const targetElem = this.dropTargetElements[i].getBoundingClientRect();
                if (elementCenter.x > targetElem.left && elementCenter.x < (targetElem.left + targetElem.width) &&
                elementCenter.y > targetElem.top && elementCenter.y < (targetElem.top + targetElem.height)) {
                    this.insertElement(this.dropTargetElements[i], 'div');
                }
            }
            
            this.deleteElements();
        }

        @HostListener('mousedown', ['$event']) onMouseDown(event: MouseEvent) {
            this.catchElement = true;
            this.createCopy();
            document.addEventListener('mousemove', this.moveElementHandler);
            document.addEventListener('mouseup', this.mouseUpHandler);
            this.dropTargetElements = this.getAllTarget();
        }       

        deleteElements() {
            this.catchElement = false;
            let copyElements = document.querySelectorAll('[copy]');
            for (let i=0;i<copyElements.length; i++) {
                copyElements[i].remove();
            }
            document.removeEventListener('mousemove', this.moveElementHandler);
            document.removeEventListener('mouseup', this.mouseUpHandler); 
        }

        moveElement(event) {
            if (this.catchElement) {
                let element: HTMLBodyElement = this.elementCopy;
                this.renderer.setStyle(element, "left", event.pageX - (element.getBoundingClientRect().width / 2) + "px");
                this.renderer.setStyle(element, "top", event.pageY - (element.getBoundingClientRect().height / 2) + "px");
            }
        }

        getAllTarget() {
            let dropTarget = document.querySelectorAll('[dropTarget]');
            return dropTarget;
        }

        createCopy() {
            let elem = this.renderer.createElement('div');
            this.renderer.setAttribute(elem, 'type', this.type);
            this.renderer.setAttribute(elem, 'copy', null);
            this.renderer.setStyle(elem, 'background-color', 'red');
            this.renderer.setStyle(elem, 'width', '100px');
            this.renderer.setStyle(elem, 'height', '100px');
            this.renderer.setStyle(elem, 'position', 'absolute');
            this.renderer.appendChild(this.elementRef.nativeElement, elem);
            this.elementCopy = elem;
        }

        insertElement(targetElement, type) {
            let elem = this.renderer.createElement(type);
            this.renderer.setStyle(elem, 'background-color', 'yellow');
            this.renderer.setStyle(elem, 'width', '90%');
            this.renderer.setStyle(elem, 'height', '10px');
            this.renderer.appendChild(targetElement, elem);
        }
}