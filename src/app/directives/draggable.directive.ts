import { Directive, ElementRef, Renderer2, HostListener, HostBinding } from "@angular/core";

@Directive({
    selector: "[draggable]"
})

export class DraggableDirective {

    catchElement: boolean = false;
    offsetX: number = 0;

    constructor(
        private elementRef: ElementRef,
        private renderer: Renderer2) {
            this.renderer.setStyle(this.elementRef.nativeElement, "position", "absolute");
        }

    moveElementHandler = (event) => {
        this.moveElement(event);
    }
    
    @HostListener('mousedown', ['$event']) onMouseDown(event: MouseEvent) {
        this.catchElement = true;
        this.offsetX = event.offsetX;
        document.addEventListener('mousemove', this.moveElementHandler);
    }

    @HostListener('mouseup', ['$event']) onMouseUp(event) {
        this.catchElement = false;
        document.removeEventListener('mousemove', this.moveElementHandler);
    }

    moveElement(event) {
        if(this.catchElement) {
            let element: HTMLBodyElement = this.elementRef.nativeElement;
            this.renderer.setStyle(element, "left", event.pageX - this.offsetX + "px");
            this.renderer.setStyle(element, "top", event.pageY - 20 + "px");
        }           
    }

}