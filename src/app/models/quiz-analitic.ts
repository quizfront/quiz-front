export class QuizAnalitic {
    id: number;
    image: string;
    name: string;
    tariff: string;
    payed: string;
    trialPeriod: number;
    request: number;
    cr: number;
    coupon: number;
}