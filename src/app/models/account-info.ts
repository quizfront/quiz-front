export class Tariff {
    id: number;
    name: string;
    cost: number;
}