export class MainSettings {
    font: {
        bold: boolean;
        italic: boolean;
        align: string;
        family: string;
        size: number;
        color: string;
    }
    background: {        
        color: string;
        showGrid: boolean;
    }
}

export class Section {
    number: number;
    height: number;
    blocks: Block[];
}

export class Block {
    number: number;
}