export class PaymentItem {
    id: number;
    date: string;
    name: string;
    type: string;
    summ: number;
    status: string;
}

export class Transaction {
    id: number;
    number: number;
    pay_way: string;
    comments: string;
    coupon: string;
    user: {
        id: number;
        name: string;
        phone: string;
        email: string;
        comments: string;
    }
    quiz: {
        id: number;
        name: string;
        type: string;
        tariff: string;
        cost: number;
    }
}