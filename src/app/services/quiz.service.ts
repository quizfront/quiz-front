import { Injectable } from '@angular/core';

import { Section } from '../models/index';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class QuizService {

  public quiz = {
    id: 0,
    name: null,
    sections: [
      {
        number: 0,
        height: 225,
        blocks: [
          {
            number: 0,
            elements: [
              {
                number: 0,
                type: 'button',
                text: 'Тест',
                styles: {
                  height: 40,
                  background: 'red'
                }
              }
            ]            
          },
          {
            number: 1,            
          },
          {
            number: 2,            
          }
        ]
      },
      {
        number: 1,
        height: 450,
        blocks: [
          {
            number: 0,            
          },
          {
            number: 1,            
          }
        ]
      }
    ]
  }

  public quizUpdated = new BehaviorSubject(null);

  constructor() { }

  // Добавление секции после секции с индексом
  addSection(number: number) {
    const section: Section = {
      number: number + 1,
      height: 900,
      blocks: [
        { number: 0 }
      ]
    }
    this.quiz.sections.splice(section.number, 0, section);
    for (let i = section.number + 1; i < this.quiz.sections.length; i++) {
      this.quiz.sections[i].number += 1;
    }
    this.quizUpdated.next(true);
  }

  deleteSection(number: number) {
    this.quiz.sections.splice(number, 1);
    for (let i = number; i < this.quiz.sections.length; i++) {
      this.quiz.sections[i].number -= 1;
    }
    this.quizUpdated.next(true);
  }

}
