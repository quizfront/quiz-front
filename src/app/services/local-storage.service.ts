import { Injectable } from '@angular/core';

@Injectable()

export class LocalStorageService {

    setToken(token: string) {
        window.localStorage.setItem("quiz_token", token);
    }

    getToken(): string {
        const token: string = window.localStorage.getItem("quiz_token");
        return token;
    }

    deleteToken() {
        window.localStorage.removeItem("quiz_token");
    }

    isNeedFaq() {
        const isNeed = window.localStorage.getItem('need_faq');
        if(isNeed == null || isNeed == '') return true;
        return false;
    }

    cancelFaq() {
        window.localStorage.setItem('need_faq', '1');
    }

    setNeedFaq() {
        window.localStorage.removeItem('need_faq');
    }
    
}