import { Injectable, ElementRef } from '@angular/core';
import * as $ from 'jquery';

@Injectable()
export class ViewService {

    private settingsOpenId: number;

    showMenu() {
        const menu: any = document.getElementsByClassName('menu-wrap')[0];
        menu.style.transform = 'translateY(0)';
    }

    hideMenu() {
        const menu: any = document.getElementsByClassName('menu-wrap')[0];
        menu.style.transform = 'translateY(-9.8rem)';
    }

    closeDropdown(index: number) {
        $('.gear').eq(index).click();
    }

    private hideAllSettingsNot(index: number) {
        $('.settings').not('.settings:eq('+index+')').width(30);
    }

    hideAllSettings() {
        $('.settings').width(30);
    }

    toggleAddItemsMenu() {
        this.hideAllSettings();
    }


    //--

    toggleSettingsMenu(index: number) {
        if (this.settingsOpenId != index) {
            $('.settings').eq(index).width(190);
            this.hideAllSettingsNot(index);
            this.settingsOpenId = index;
        } else {
            $('.settings').eq(index).width(30);
            this.settingsOpenId = null;
        }
    }

    hideAllSettingsMenues() {
        $('.settings').width(30);
        this.settingsOpenId = null;
    }

}