import { Injectable } from '@angular/core';

import { LocalStorageService } from './local-storage.service';

@Injectable()
export class AuthService {

    private login: string = 'admin';
    private password: string = 'admin';

    constructor(private ls: LocalStorageService) {}

    isLogged(): boolean {
        const token: string = this.ls.getToken();
        if (token) {
            return true;
        }
        return false;
    }

    authorize(login: string, password: string) {
        if (login == this.login && password == this.password) {
            this.ls.setToken('temporary_token_for_tests');
        }
    }

    logout() {
        this.ls.deleteToken();
    }

// массив афилатов для partners
    authAffilatesData = [
        {
          name: "Василий",
          quiz: 3,
          paid_quiz: 1,
          profit: 500,
          date: '2017-01-30'
        },
        {
          name: "Надежда",
          quiz: 2,
          paid_quiz: 2,
          profit: 1000,
          date: '2018-03-13'
        },
        {
          name: "Ольга",
          quiz: 1,
          paid_quiz: 1,
          profit: 500,
          date: '2018-02-19'
        },
        {
          name: "Николай",
          quiz: 1,
          paid_quiz: 2,
          profit: 700,
          date: '2018-03-08'
        },
        {
          name: "Александр",
          quiz: 10,
          paid_quiz: 6,
          profit: 1500,
          date: '2018-01-22'
        }
    ]


}
