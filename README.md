# Quiz

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.1.

##  Install dependencies

After download project to your local computer you need to install all dependencies. For that do `npm install` command in CLI

## Development server

Run `ng serve -o` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng g c component-name --spec=false` to generate a new component without spec file.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
